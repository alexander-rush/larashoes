# LARASHOES
## Pages

The header will have a logo and a menu with the following pages: 

- MEN's
- WOMAN's
- About
- Contacts
- Cart

Also in the header there will be buttons LOGIN and REGISTER or Profile_name depending on whether the user is authorized or not, which will be located in the upper right corner of the site header.

## Features

1. The men's and WOMAN's pages will display shoes as a list. All shoes can be filtered by **name**, by **price** (ascending or descending), by **color**, by **brand**, by **category** (e.g. winter shoes, sports shoes, etc.), **by number of comments** (discussed) and by **rating**.
2. Each Shoe can be added to the cart by specifying the number. Items that are not available cannot be added to the cart.
3. On the Cart page, you can place an order for the selected products, if the cart is empty, the order will not be available. It will also be possible to remove the product from the basket.
4. Unauthorized users (guest) will only be able to view the product without the ability to add it to the cart.
5. After the purchase, you can leave a review about the purchased goods, comments to reviews can be left only by authorized users, guests in turn can only view them